﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Hero
    {
        private string name;
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
        public Hero(string Name)
        {
            this.name = Name;
        }
        public Hero()
        {

        }
    }
    enum Fist
    {
        石头 = 1,
        剪刀,
        布
    }
    enum Yx
    {
        刘备 = 1,
        孙权,
        曹操
    }

    class Program
    {
        //玩家姓名
        static string xm;
        //对手姓名
        static Yx name;
        //实例化随机数
        static Random sz = new Random();
        //计算分数
        static int playersum = 0, robotsum = 0, gamesum = 0;
        static void Main(string[] args)
        {
            Console.WriteLine("---------------欢迎进入游戏世界---------------");
            Console.WriteLine();
            Console.WriteLine("*************************************");
            Console.WriteLine("**********猜拳··开始！！！*********");
            Console.WriteLine("*************************************");
            Console.WriteLine("出拳规则：1.剪刀2.石头3.布");
            pk();
            Console.ReadKey();
        }
        static string listfist()
        {
            string fiststring = "";
            int i = 1;
            foreach (Fist f in Enum.GetValues(typeof(Fist)))
            {
                fiststring += i + "、" + f + ";";
                i++;
            }
            return fiststring;
        }
        static string listyx()
        {
            string yxstring = "";
            int i = 1;
            foreach (Yx f in Enum.GetValues(typeof(Yx)))
            {
                yxstring += i + "、" + f + ";";
                i++;
            }
            return yxstring;
        }
        static void pk()
        {
            Console.WriteLine("请选择对方角色{0}", listyx());
            int jx = int.Parse(Console.ReadLine());
            name = (Yx)jx;
            Console.WriteLine("请输入你的姓名：");
            xm = Console.ReadLine();
            Console.WriteLine("{0} vs {1} 对战！", xm, name);
            Console.WriteLine("开始游戏吗？(y/n)");
            string begin = Console.ReadLine();
            if (begin == "y")
            {
                confrontation();
            }
            else if (begin == "n")
            {
                end();
            }
        }
        static void confrontation()
        {
            Console.WriteLine("请出拳：{0}(输入相应数字：)", listfist());
            int guess = int.Parse(Console.ReadLine());
            //我方出拳
            Fist fname = (Fist)guess;
            //对手出拳
            int cq = sz.Next(1, 4);
            Fist q = (Fist)cq;
            int qq = (int)q;
            switch (guess)
            {
                case 1:
                    Console.WriteLine("{0}:出拳：{1}", xm, fname);
                    Console.WriteLine("{0}:出拳：{1}", name, q);
                    if (qq.Equals(3))
                    {
                        Console.WriteLine("{0}输了，不要害怕失败大不了从头来过！",xm);
                        robotsum++;
                        gamesum++;
                    }
                    else if (fname.Equals(q))
                    {
                        Console.WriteLine("和局,真倒霉！你个王八犊子等着瞧！");
                        gamesum++;
                    }
                    else
                    {
                        Console.WriteLine("芜湖！{0}赢了！",xm);
                        playersum++;
                        gamesum++;
                    }
                    circulation();
                    break;
                case 2:
                    Console.WriteLine("{0}:出拳：{1}", xm, fname);
                    Console.WriteLine("{0}:出拳：{1}", name, q);
                    if (qq.Equals(1))
                    {
                        Console.WriteLine("{0}输了，不要害怕失败大不了从头来过！", xm);
                        robotsum++;
                        gamesum++;
                    }
                    else if (fname.Equals(q))
                    {
                        Console.WriteLine("和局,真倒霉！你个王八犊子等着瞧！");
                        gamesum++;
                    }
                    else
                    {
                        Console.WriteLine("芜湖！{0}赢了！",xm);
                        playersum++;
                        gamesum++;
                    }
                    circulation();
                    break;
                case 3:
                    Console.WriteLine("{0}:出拳：{1}", xm, fname);
                    Console.WriteLine("{0}:出拳：{1}", name, q);
                    if (qq.Equals(2))
                    {
                        Console.WriteLine("{0}输了，不要害怕失败大不了从头来过！", xm);
                        robotsum++;
                        gamesum++;
                    }
                    else if (fname.Equals(q))
                    {
                        Console.WriteLine("和局,真倒霉！你个王八犊子等着瞧！");
                        gamesum++;
                    }
                    else
                    {
                        Console.WriteLine("芜湖！{0}赢了！",xm);
                        playersum++;
                        gamesum++;
                    }
                    circulation();
                    break;
                default:
                    Console.WriteLine("没有该选项！");
                    break;
            }
        }
        static void end()
        {
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("====================================");
            Console.WriteLine("{0} vs {1}", name, xm);
            Console.WriteLine("对战次数：{0}",gamesum);
            Console.WriteLine();
            Console.WriteLine("姓名\t得分");
            Console.WriteLine("{0}\t{1}", xm,playersum);
            Console.WriteLine("{0}\t{1}", name,robotsum);
            if (playersum>robotsum)
            {
                Console.WriteLine("结果：{0}赢，{1}输了", xm, name) ;
            }
            else if(playersum < robotsum)
            {
                Console.WriteLine("结果：{0}赢，{1}输了",name,xm);
            }
            else
            {
                Console.WriteLine("结果：似乎平局了哦");
            }
            cx:
            Console.WriteLine("要开始下一局吗？(y/n)");
            string newgame = Console.ReadLine();
            if (newgame == "y")
            {
                playersum = 0; robotsum = 0; gamesum = 0;
                pk();
            }
            else if (newgame == "n")
            {
                Console.WriteLine("系统退出");
            }
            else
            {
                Console.WriteLine("没有该选项请重新选择");
                goto cx;
            }
        }
        static void circulation()
        {
            cx:
            Console.WriteLine("是否开启下一轮？(y/n)");
            string next = Console.ReadLine();
            if (next == "y")
            {
                confrontation();
            }
            else if (next == "n")
            {
                end();
            }
            else
            {
                Console.WriteLine("没有该选项!重新选择！");
                goto cx;
            }
        }
    }
}
